# Abstract

_Provide build information and pretty-printed GitLab security reports in a CI pipeline_

!!! danger "Exposes found weaknesses and vulnerabilities"

    Publishing scan results can pose a security risk,
    consider using **glsr-present** in trusted environments only.


While GitLab offers various [security checks]
for projects using a CI pipeline, results are provided as JSON files
that contain the findings in a machine-readable way rather than
human-readable (unless on [GitLab Ultimate] subscriptions).

This project tries to close that gap by pretty-printing the reports
alongside some basic information about the build and providing them
through pages or similar Docs-as-Code solutions.

The reports are visualized using HTML, JS and CSS helper files
embedded into the package (these files were developed
by Oliver Haase and Rainer Schwarzbach in early 2023).

When calling the module as a script (`glsr-present`),
the helper files are copied to the destination directory
if any GitLab security reports (`gl-*-report.json`)
are found in the working directory.

!!! note "Currently supported reports"

    - [Container Scanning]
    - [IaC Scanning]
    - [Secret Detection]
    - [Static Application Security Testing (SAST)]


Additionally, an overview page containing human-readable build information
is generated in the destination directory using a built-in
(or user-provided with `-f` / `--template-file`)
[Jinja template],
as well as a JSON file containing build information for machine processing
(eg. used by the JavaScript functions to link found issues to the related
places in the examined project’s source code).

The destination directory defaults to the `docs` subdirectory
of the current working directory, but can be changed using the
`-o` (`--output-directory`) option.

Using `-r` (`--reports-path`), you can select a different directory
to read the reports from than the default (current working directory).

For skipping the reports and just providing basic build information,
there is the `-s` (`--skip-reports`) switch that could eg. be used
in release pipelines to prevent disclosing security information.


## Usage options

### Installation from PyPI

``` bash
pip install glsr-present
```

[Jinja2] Version 3.1.2 (or newer) is required
 and will be installed as an indirect dependency if not installed yet.

Installation in a virtual environment is strongly recommended.


### Run through uvx directly

If you already use [uv], you can simply run **glsr-present**
through **uvx** without prior install:

``` bash
uvx glsr-present
```


## Help message

Output of `glsr-present --help`:

```text
usage: glsr-present [-h] [--version] [-d | -v | -q]
                    [-b TEMPLATE_NAME | -f TEMPLATE_PATH] [-l] [-n]
                    [-o DESTINATION] [-r DIRECTORY] [-s]

Provide build information and pretty-printed GitLab security reports in a CI
pipeline

options:
  -h, --help            show this help message and exit
  --version             print version and exit
  -l, --list-templates  list available templates and exit
  -n, --dry-run         no action (dry run): do not write any files
  -o DESTINATION, --output-directory DESTINATION
                        write files to directory DESTINATION (default: docs)
  -r DIRECTORY, --reports-path DIRECTORY
                        read reports from DIRECTORY (default:
                        /home/rainer/git/glsr-present)
  -s, --skip-reports    skip reading reports, just provide build information

Logging options:
  control log level (default is WARNING)

  -d, --debug           output all messages (log level DEBUG)
  -v, --verbose         be more verbose (log level INFO)
  -q, --quiet           be more quiet (log level ERROR)

Template option:
  Select a builtin template or one from the file system for the overview
  page

  -b TEMPLATE_NAME, --builtin-template TEMPLATE_NAME
                        use the built-in template TEMPLATE_NAME (default:
                        build-info.md.j2)
  -f TEMPLATE_PATH, --template-file TEMPLATE_PATH
                        use the template from file TEMPLATE_PATH
```


* * *
[security checks]: https://docs.gitlab.com/ee/user/application_security/secure_your_application.html
[GitLab Ultimate]: https://about.gitlab.com/pricing/ultimate/
[Container Scanning]: https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html
[IaC Scanning]: https://docs.gitlab.com/ee/user/application_security/iac_scanning/index.html
[Secret Detection]: https://docs.gitlab.com/ee/user/application_security/secret_detection/index.html
[Static Application Security Testing (SAST)]: https://docs.gitlab.com/ee/user/application_security/sast/index.html
[Jinja template]: https://jinja.palletsprojects.com/en/3.1.x/templates/
[Jinja2]: https://pypi.org/project/Jinja2/
[uv]: https://docs.astral.sh/uv/
